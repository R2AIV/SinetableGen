#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#define PI 3.141592

int main(int argc, char **argv)
{
	printf("SINGEN: sinetable generator for PMSM motor control\r\n");
	printf("Denis S. Sovkov 12.07.2018\r\n");

	unsigned int ArraySize=0x00;
	unsigned int MaxValue=0x00;
	
	unsigned int UValue=0x00;
	unsigned int VValue=0x00;
	unsigned int WValue=0x00;

	FILE *OutFile;


	if(argc != 4)
	{
		printf("USAGE: singen [array_size] [max_value] [filename]\r\n");
		return 0;
	}

	ArraySize=atoi(argv[1]);
	MaxValue=atoi(argv[2]);

	if(ArraySize==0 || MaxValue==0)
	{
		printf("Incorrect parameters!\r\n");
		return -1;
	}


	OutFile=fopen(argv[3],"w");
	fprintf(OutFile,"uint8_t SineTable[3][%d]\r\n{\r\n",ArraySize);
	printf("Generating sinetable file...");

	for(float i=0.0;i<=ArraySize;i++)
	{
		float RadianAngle=i*PI/180.0*360/ArraySize;

		UValue=(sin(RadianAngle)*MaxValue)/2+MaxValue/2;
		VValue=(sin(RadianAngle+(120*PI/180))*MaxValue)/2+MaxValue/2;
		WValue=(sin(RadianAngle+(240*PI/180))*MaxValue)/2+MaxValue/2;		

		fprintf(OutFile,"\t%.3d, %.3d, %.3d,\r\n",UValue,VValue,WValue);
	}

	fprintf(OutFile,"};\r\n\r\n");
	printf("DONE!\r\n");
	fclose(OutFile);


	return 0;
}

